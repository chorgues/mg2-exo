<?php

namespace Absolunet\Exercice1Be\Plugin;

use Magento\Theme\Block\Html\Header;

/**
 * Header plugin to add "Hello World".
 */
class HeaderPlugin
{
    public function afterToHtml(Header $subject, $result)
    {
        return '<li>' . __('Hello World') . '</li>' . $result;
    }
}
