<?php

declare(strict_types=1);

namespace Absolunet\Exercice2Be\Plugin\Model;

use Magento\Review\Model\Review;

class ReviewPlugin
{
    public function afterValidate(Review $subject, $result)
    {
        $new_errors = is_array($result) ? $result : [];
        $nickname = $subject->getNickname();
        if (is_string($nickname) && strpos($nickname, '-') !== false) {
            $new_errors[] = __("Nickname can't contain '-'.");
        }

        return empty($new_errors) ? true : $new_errors;
    }
}
