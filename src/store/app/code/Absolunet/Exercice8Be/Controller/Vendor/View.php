<?php

declare(strict_types=1);

namespace Absolunet\Exercice8Be\Controller\Vendor;

use Absolunet\Exercice7Be\Model\VendorRepository;
use Absolunet\Exercice8Be\Registry\Vendor;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class vendor view
 * @package Absolunet\Exercice8Be\Controller\Vendor
 */
class View implements HttpGetActionInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    private $redirectFactory;
    /**
     * @var \Absolunet\Exercice7Be\Model\VendorRepository
     */
    private $vendorRepository;
    /**
     * @var \Absolunet\Exercice8Be\Registry\Vendor
     */
    private $vendorRegistry;

    /**
     * View constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Absolunet\Exercice7Be\Model\VendorRepository $vendorRepository
     * @param \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
     * @param \Absolunet\Exercice8Be\Registry\Vendor $vendorRegistry
     */
    public function __construct(
        RequestInterface $request,
        PageFactory $resultPageFactory,
        VendorRepository $vendorRepository,
        RedirectFactory $redirectFactory,
        Vendor $vendorRegistry
    ) {
        $this->request = $request;
        $this->resultPageFactory = $resultPageFactory;
        $this->vendorRepository = $vendorRepository;
        $this->redirectFactory = $redirectFactory;
        $this->vendorRegistry = $vendorRegistry;
    }

    /**
     * Show vendor
     * @inheritDoc
     */
    public function execute(): ResultInterface
    {
        $resultPage = $this->resultPageFactory->create();

        $vendorParam = $this->request->getParam('vendor', null);

        if (!$vendorParam) {
            return $this->redirectListing();
        }

        $vendor = $this->vendorRepository->get((int)$vendorParam);

        if (!$vendor->getId()) {
            return $this->redirectListing();
        }
        $this->vendorRegistry->setCurrentVendor($vendor);

        return $resultPage;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    private function redirectListing(): ResultInterface
    {
        $redirect = $this->redirectFactory->create();
        return $redirect->setPath('*/*/listing');
    }
}
