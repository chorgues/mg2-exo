<?php

namespace Absolunet\Exercice8Be\ViewModel\Vendor;

use Absolunet\Exercice7Be\Model\ResourceModel\Vendor\Collection;
use Absolunet\Exercice7Be\Model\ResourceModel\Vendor\CollectionFactory;
use Absolunet\Exercice7Be\Model\Vendor;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\UrlFactory;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Listing implements ArgumentInterface
{
    /**
     * @var \Absolunet\Exercice7Be\Model\ResourceModel\Vendor\CollectionFactory
     */
    public $vendorCollectionFactory;

    /**
     * @var \Absolunet\Exercice7Be\Model\ResourceModel\Vendor\Collection
     */
    private $vendorCollection;

    /**
     * @var \Magento\Framework\UrlFactory
     */
    private $urlFactory;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * Listing constructor.
     * @param \Absolunet\Exercice7Be\Model\ResourceModel\Vendor\CollectionFactory $vendorCollectionFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\UrlFactory $urlFactory
     */
    public function __construct(
        CollectionFactory $vendorCollectionFactory,
        RequestInterface $request,
        UrlFactory $urlFactory
    ) {
        $this->vendorCollectionFactory = $vendorCollectionFactory;
        $this->urlFactory = $urlFactory;
        $this->request = $request;
    }

    /**
     * @return \Absolunet\Exercice7Be\Model\ResourceModel\Vendor\Collection|null
     */
    public function getVendors(): ?Collection
    {
        if ($this->vendorCollection) {
            return $this->vendorCollection;
        }

        $this->vendorCollection = $this->vendorCollectionFactory->create();

        $this->setOrderFromRequest();

        return $this->vendorCollection;
    }

    private function setOrderFromRequest(): void
    {
        $direction = $this->request->getParam('order', Collection::SORT_ORDER_ASC);
        if (Collection::SORT_ORDER_ASC !== $direction && Collection::SORT_ORDER_DESC !== $direction) {
            $direction = Collection::SORT_ORDER_ASC;
        }
        $this->vendorCollection->setOrder('name', $direction);
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        $direction = $this->request->getParam('order', Collection::SORT_ORDER_ASC);
        if (Collection::SORT_ORDER_ASC !== $direction && Collection::SORT_ORDER_DESC !== $direction) {
            $direction = Collection::SORT_ORDER_ASC;
        }
        return $direction;
    }

    /**
     * @param \Absolunet\Exercice7Be\Model\Vendor $vendor
     * @return string
     */
    public function getVendorUrl(Vendor $vendor): string
    {
        $url = $this->urlFactory->create();

        return $url->getRouteUrl('vendor/vendor/view', ['vendor' => $vendor->getId()]);
    }

    /**
     * @return array[]
     */
    public function getOrderList(): array
    {
        return [
            ['name' => __('Name ASC'), 'value' => Collection::SORT_ORDER_ASC],
            ['name' => __('Name DESC'), 'value' => Collection::SORT_ORDER_DESC]
        ];
    }
}
