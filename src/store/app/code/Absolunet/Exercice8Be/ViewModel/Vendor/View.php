<?php

namespace Absolunet\Exercice8Be\ViewModel\Vendor;

use Absolunet\Exercice7Be\Model\Vendor;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class View implements ArgumentInterface
{
    /**
     * @var \Absolunet\Exercice8Be\Registry\Vendor
     */
    private $vendorRegistry;

    /**
     * View constructor.
     * @param \Absolunet\Exercice8Be\Registry\Vendor $vendorRegistry
     */
    public function __construct(
        \Absolunet\Exercice8Be\Registry\Vendor $vendorRegistry
    ) {
        $this->vendorRegistry = $vendorRegistry;
    }

    /**
     * @return \Absolunet\Exercice7Be\Model\Vendor|null
     */
    public function getVendor(): ?Vendor
    {
        return $this->vendorRegistry->getCurrentVendor();
    }
}
