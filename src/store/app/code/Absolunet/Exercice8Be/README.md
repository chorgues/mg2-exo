# Absolunet Exercice8Be

Create a page in the storefront where all vendors are listed. Add the ability to sort/filter by vendor. Create a page of vendor details information. This page should also show a list of all products assigned to each vendor.
