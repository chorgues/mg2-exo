<?php
/**
 * @author    Clement Horgues <chorgues@absolunet.com>
 * @copyright Copyright (c) Absolunet (http://www.absolunet.com)
 * @link      http://www.absolunet.com
 */

declare(strict_types=1);

namespace Absolunet\Exercice8Be\Registry;

/**
 * Class Vendor
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class Vendor
{
    /** @var \Absolunet\Exercice7Be\Model\Vendor */
    public $currentVendor;

    /**
     * @return \Absolunet\Exercice7Be\Model\Vendor|null
     */
    public function getCurrentVendor(): ?\Absolunet\Exercice7Be\Model\Vendor
    {
        return $this->currentVendor;
    }

    /**
     * @param \Absolunet\Exercice7Be\Model\Vendor $currentVendor
     */
    public function setCurrentVendor(\Absolunet\Exercice7Be\Model\Vendor $currentVendor): void
    {
        $this->currentVendor = $currentVendor;
    }
}
