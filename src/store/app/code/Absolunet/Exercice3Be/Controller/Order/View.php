<?php
declare(strict_types=1);

namespace Absolunet\Exercice3Be\Controller\Order;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Controller\AbstractController\OrderViewAuthorizationInterface;

/**
 * Class Order Json
 * @package Absolunet\Exercice3Be\Controller\Order
 */
class View implements HttpGetActionInterface
{
    /**
     * @var RequestInterface
     */
    public $request;
    /**
     * @var JsonFactory
     */
    public $resultJsonFactory;

    /**
     * @var OrderRepositoryInterface
     */
    public $orderLoader;

    /**
     * @var OrderViewAuthorizationInterface
     */
    public $orderViewAuthorization;

    /**
     * @param RequestInterface $request
     * @param JsonFactory $resultJsonFactory
     * @param OrderRepositoryInterface $orderLoader
     * @param OrderViewAuthorizationInterface $orderViewAuthorization
     */
    public function __construct(
        RequestInterface $request,
        JsonFactory $resultJsonFactory,
        OrderRepositoryInterface $orderLoader,
        OrderViewAuthorizationInterface $orderViewAuthorization
    ) {
        $this->request = $request;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->orderLoader = $orderLoader;
        $this->orderViewAuthorization = $orderViewAuthorization;
    }

    /**
     * Get order json
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $resultJson = $this->resultJsonFactory->create();

        try {
            $order = $this->getOrder();
        } catch (InputException $e) {
            return $resultJson->setData(['error' => __("orderId cannot be empty or 0.")]);
        } catch (NoSuchEntityException $e) {
            return $resultJson->setData(['error' => __("orderId doesn't exist or you do not own the order.")]);
        } catch (\Exception $e) {
            return $resultJson->setData(['error' => __($e->getMessage())]);
        }

        $allItems = $order->getAllItems();
        $items = [];
        foreach ($allItems as $item) {
            $items[] = [
                'sku' => $item->getSku(),
                'item_id' => $item->getItemId(),
                'price' => $item->getPrice()
            ];
        }

        return $resultJson->setData([
            'orderId' => $order->getEntityId(),
            'status' => $order->getStatus(),
            'total' => $order->getGrandTotal(),
            'products' => $items,
            'invoicedTotal' => $order->getTotalInvoiced()
        ]);
    }

    /**
     * @return OrderInterface
     * @throws NoSuchEntityException
     * @throws InputException
     */
    public function getOrder(): OrderInterface
    {
        $orderId = (int)$this->request->getParam('orderId', null);
        $order = $this->orderLoader->get($orderId);

        /**
         * Should use OrderViewAuthorizationInterface::canView(Order) to secure informations
         */
        if (!$this->orderViewAuthorization->canView($order)) {
            throw new NoSuchEntityException();
        }

        return $order;
    }
}
