<?php

namespace Absolunet\Exercice7Be\Setup\Patch\Data;

use Absolunet\Exercice7Be\Model\Vendor;
use Absolunet\Exercice7Be\Model\VendorFactory;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class Populate implements DataPatchInterface
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilderFactory
     */
    private $searchCriteriaBuilderFactory;
    /**
     * @var \Absolunet\Exercice7Be\Model\VendorFactory
     */
    private $vendorFactory;

    /**
     * Populate constructor.
     * @param \Absolunet\Exercice7Be\Model\VendorFactory $vendorFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Framework\Api\SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
     */
    public function __construct(
        VendorFactory $vendorFactory,
        ProductRepositoryInterface $productRepository,
        ModuleDataSetupInterface $moduleDataSetup,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
    ) {
        $this->vendorFactory = $vendorFactory;
        $this->productRepository = $productRepository;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
    }

    public static function getDependencies(): array
    {
        return [];
    }

    public function getAliases(): array
    {
        return [];
    }

    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $connection->startSetup();

        $vendorsData = [
            'Sample Vendor 1',
            'Sample Vendor 2',
            'Sample Vendor 3',
            'Sample Vendor 4',
        ];

        $vendorIds = [];

        foreach ($vendorsData as $vendorsDatum) {
            $vendor = $this->createVendor();
            $vendor->setData('name', $vendorsDatum);
            $vendorIds[] = $vendor->save()->getId();
        }

        $searchCriteriaBuilder = $this->searchCriteriaBuilderFactory->create();
        $searchCriteriaBuilder->setPageSize(10);
        $searchCriteriaBuilder->addFilter(
            ProductInterface::STATUS,
            Status::STATUS_ENABLED
        );
        $productList = $this->productRepository->getList($searchCriteriaBuilder->create());
        $count = min($productList->getTotalCount(), 10);

        $arrayMap = [];
        foreach (array_slice($productList->getItems(), 0, $count) as $key => $product) {
            $arrayMap[$key] = $product->getId();
        }
        $productsId = $arrayMap;

        $array = [];
        foreach ($vendorIds as $vendorId) {
            foreach ($productsId as $productId) {
                $array[] = [$vendorId, $productId];
            }
        }

        $connection->insertArray('exercice7be_vendor2product', ['vendor_id', 'product_id'], $array);

        $connection->endSetup();
    }

    /**
     * @return \Absolunet\Exercice7Be\Model\Vendor
     */
    private function createVendor(): Vendor
    {
        return $this->vendorFactory->create();
    }
}
