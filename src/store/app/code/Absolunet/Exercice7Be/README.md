# Absolunet Exercice7Be

Create a new entity for “vendor”. Create a new table (exercice7be_vendor, with two fields for vendor_id, name), and create a data install/upgrade script to connect vendors with products (exercice7be_vendor2product, using many-to-many connections). On the product view page, show a block with a list of assigned vendors.
