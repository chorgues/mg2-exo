<?php

namespace Absolunet\Exercice7Be\Model;

use Absolunet\Exercice7Be\Model\ResourceModel\VendorFactory as ResourceVendorFactory;
use Magento\Catalog\Api\Data\ProductSearchResultsInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

/**
 * Vendor resource module
 * @method array getName()
 * @method array getProductIds()
 */
class Vendor extends AbstractModel implements IdentityInterface
{
    public const CACHE_TAG = 'vendor';

    /**
     * @var \Absolunet\Exercice7Be\Model\ResourceModel\VendorFactory
     */
    private $vendorFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilderFactory
     */
    private $searchCriteriaBuilderFactory;

    /**
     * Vendor constructor.
     * @param \Absolunet\Exercice7Be\Model\ResourceModel\VendorFactory $vendorFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        ResourceVendorFactory $vendorFactory,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->vendorFactory = $vendorFactory;
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function getProducts(): ProductSearchResultsInterface
    {
        $searchCriteriaBuilder = $this->searchCriteriaBuilderFactory->create();
        return $this->productRepository->getList(
            $searchCriteriaBuilder->addFilter('entity_id', $this->getProductIds(), 'in')->create()
        );
    }

    /**
     * @return string[]
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @SuppressWarnings(PHPMD.ProtectedMethod)
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\Vendor::class);
    }
}
