<?php

namespace Absolunet\Exercice7Be\Model;

use Absolunet\Exercice7Be\Model\ResourceModel\Vendor\CollectionFactory;
use Absolunet\Exercice7Be\Model\ResourceModel\VendorFactory as ResourceVendorFactory;
use Absolunet\Exercice7Be\Model\VendorFactory as VendorModelFactory;

/**
 *
 */
class VendorRepository
{
    /**
     * @var \Absolunet\Exercice7Be\Model\ResourceModel\VendorFactory
     */
    private $resourceVendorFactory;
    /**
     * @var \Absolunet\Exercice7Be\Model\ResourceModel\Vendor\CollectionFactory
     */
    private $vendorCollectionFactory;
    /**
     * @var \Absolunet\Exercice7Be\Model\VendorFactory
     */
    private $vendorFactory;

    /**
     * VendorRepository constructor.
     * @param \Absolunet\Exercice7Be\Model\VendorFactory $vendorFactory
     * @param \Absolunet\Exercice7Be\Model\ResourceModel\VendorFactory $resourceVendorFactory
     * @param \Absolunet\Exercice7Be\Model\ResourceModel\Vendor\CollectionFactory $vendorCollectionFactory
     */
    public function __construct(
        VendorModelFactory $vendorFactory,
        ResourceVendorFactory $resourceVendorFactory,
        CollectionFactory $vendorCollectionFactory
    ) {
        $this->vendorFactory = $vendorFactory;
        $this->resourceVendorFactory = $resourceVendorFactory;
        $this->vendorCollectionFactory = $vendorCollectionFactory;
    }

    /**
     * @param int $productId
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByProductId(int $productId): array
    {
        $vendorRM = $this->resourceVendorFactory->create();

        $vendors = $vendorRM->getVendors($productId);

        $vendorCollection = $this->vendorCollectionFactory->create();
        $vendorCollection->addFieldToFilter('vendor_id', ['in' => array_column($vendors, 'vendor_id')]);

        return $vendorCollection->getItems();
    }

    /**
     * @param int $vendorId
     * @return \Absolunet\Exercice7Be\Model\Vendor
     */
    public function get(int $vendorId): Vendor
    {
        $vendorRM = $this->resourceVendorFactory->create();
        $vendor = $this->vendorFactory->create();
        $vendorRM->load($vendor, $vendorId);

        return $vendor;
    }
}
