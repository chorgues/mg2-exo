<?php

namespace Absolunet\Exercice7Be\Model\ResourceModel\Vendor;

use Absolunet\Exercice7Be\Model\ResourceModel\Vendor;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Vendor Resource Collection
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize vendor resource model
     *
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @SuppressWarnings(PHPMD.ProtectedMethod)
     */
    protected function _construct(): void
    {
        $this->_init(
            \Absolunet\Exercice7Be\Model\Vendor::class,
            Vendor::class
        );
    }
}
