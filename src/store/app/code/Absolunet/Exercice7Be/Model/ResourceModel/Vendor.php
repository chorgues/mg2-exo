<?php

declare(strict_types=1);

namespace Absolunet\Exercice7Be\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * @author    Clement Horgues <chorgues@absolunet.com>
 * @copyright Copyright (c) Absolunet (http://www.absolunet.com)
 * @link      http://www.absolunet.com
 */

/**
 * Vendor resource module
 *
 */
class Vendor extends AbstractDb
{
    /**
     * @var string
     */
    private $vendorProducts;

    /**
     * Get vendors from product id
     * @param int $productId
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getVendors(int $productId): array
    {
        $connection = $this->getConnection();

        $select = $connection->select()
            ->from(['vendor2product' => $this->vendorProducts], [])
            ->where('vendor2product.product_id = ?', $productId)
            ->order('vendor2product.vendor_id');

        $select
            ->joinInner(
                ['vendor' => $this->getMainTable()],
                'vendor2product.vendor_id = vendor.vendor_id',
                ['vendor.vendor_id', 'vendor.name']
            );

        return $connection->fetchAssoc($select);
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return \Absolunet\Exercice7Be\Model\ResourceModel\Vendor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @SuppressWarnings(PHPMD.ProtectedMethod)
     */
    protected function _afterLoad(AbstractModel $object): Vendor
    {
        $object->setData('product_ids', $this->getProducts((int)$object->getId()));
        return parent::_afterLoad($object);
    }

    /**
     * Get products id from vendor id
     * @param int $vendorId
     * @return array
     */
    public function getProducts(int $vendorId): array
    {
        $connection = $this->getConnection();

        $select = $connection->select()
            ->from(['vendor2product' => $this->vendorProducts], 'vendor2product.product_id')
            ->where('vendor2product.vendor_id = ?', $vendorId)
            ->order('vendor2product.product_id');

        return $connection->fetchCol($select);
    }

    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @SuppressWarnings(PHPMD.ProtectedMethod)
     */
    protected function _construct()
    {
        $this->_init('exercice7be_vendor', 'vendor_id');
        $this->vendorProducts = $this->getTable('exercice7be_vendor2product');
    }
}
