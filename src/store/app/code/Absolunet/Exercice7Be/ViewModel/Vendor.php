<?php

namespace Absolunet\Exercice7Be\ViewModel;

use Absolunet\Exercice7Be\Model\VendorRepository;
use Magento\Catalog\Helper\Data;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Vendor implements ArgumentInterface
{
    public $vendorRepository;
    public $catalogData;

    /**
     * Vendors constructor.
     * @param \Magento\Catalog\Helper\Data $catalogData
     * @param \Absolunet\Exercice7Be\Model\VendorRepository $vendorRepository
     */
    public function __construct(
        Data $catalogData,
        VendorRepository $vendorRepository
    ) {
        $this->vendorRepository = $vendorRepository;
        $this->catalogData = $catalogData;
    }

    /**
     * @return string[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getVendors(): array
    {
        $product = $this->catalogData->getProduct();
        if ($product) {
            $arrayMap = [];
            foreach ($this->vendorRepository->getByProductId((int)$product->getId()) as $key => $vendor) {
                $arrayMap[$key] = $vendor->getName();
            }
            return $arrayMap;
        }
        return [];
    }
}
