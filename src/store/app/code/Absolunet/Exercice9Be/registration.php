<?php
/**
 * @author    Clement Horgues <chorgues@absolunet.com>
 * @copyright Copyright (c) Absolunet (http://www.absolunet.com)
 * @link      http://www.absolunet.com
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Absolunet_Exercice9Be',
    __DIR__
);
