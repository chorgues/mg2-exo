<?php
/**
 * @author    Clement Horgues <chorgues@absolunet.com>
 * @copyright Copyright (c) Absolunet (http://www.absolunet.com)
 * @link      http://www.absolunet.com
 */

namespace Absolunet\Exercice9Be\Setup\Patch\Data;

use Absolunet\Exercice9Be\Model\Product\Attribute\Backend\Warranty as BackendWarranty;
use Absolunet\Exercice9Be\Model\Product\Attribute\Frontend\Warranty as FrontendWarranty;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class AddWarrantyProductAttribute implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;
    /**
     * @var \Magento\Eav\Api\AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilderFactory
     */
    private $searchCriteriaBuilderFactory;

    /**
     * AddWarrantyProductAttribute constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     * @param \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        AttributeSetRepositoryInterface $attributeSetRepository,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
    }

    public static function getDependencies(): array
    {
        return [];
    }

    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return \Absolunet\Exercice9Be\Setup\Patch\Data\AddWarrantyProductAttribute|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply(): void
    {
        $this->moduleDataSetup->startSetup();

        $eavSetup = $this->eavSetupFactory->create();
        $searchCriteriaBuilder = $this->searchCriteriaBuilderFactory->create();

        $attributeSet = $this->attributeSetRepository->getList(
            $searchCriteriaBuilder->addFilter('attribute_set_name', 'Gear')->create()
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'warranty',
            [
                'group' => 'General',
                'type' => 'text',
                'backend' => BackendWarranty::class,
                'frontend' => FrontendWarranty::class,
                'label' => 'Warranty length (years)',
                'input' => 'text',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'is_html_allowed_on_front' => true
            ]
        );

        $attributeSet = $attributeSet->getItems();
        if (!empty($attributeSet)) {
            $attributeSet = reset($attributeSet);
            $eavSetup->addAttributeToSet(
                \Magento\Catalog\Model\Product::ENTITY,
                'Gear',
                'General',
                'warranty'
            );
        }

        $this->moduleDataSetup->endSetup();
    }
}
