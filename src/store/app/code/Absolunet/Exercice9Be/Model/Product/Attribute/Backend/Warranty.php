<?php
/**
 * @author    Clement Horgues <chorgues@absolunet.com>
 * @copyright Copyright (c) Absolunet (http://www.absolunet.com)
 * @link      http://www.absolunet.com
 */

namespace Absolunet\Exercice9Be\Model\Product\Attribute\Backend;

use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;

/**
 * Warranty attribute backend
 */
class Warranty extends AbstractBackend
{
    /**
     * Before save
     *
     * @param \Magento\Framework\DataObject $object
     * @return $this
     */
    public function beforeSave($object): Warranty
    {
        $warranty = (int)$object->getData('warranty');
        if ($warranty === 1) {
            $object->setData('warranty', $warranty . ' year');
            return $this;
        }

        if ($warranty > 1) {
            $object->setData('warranty', $warranty . ' years');
            return $this;
        }

        return parent::beforeSave($object);
    }
}
