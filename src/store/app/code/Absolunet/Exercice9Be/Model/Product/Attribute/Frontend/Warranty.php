<?php
/**
 * @author    Clement Horgues <chorgues@absolunet.com>
 * @copyright Copyright (c) Absolunet (http://www.absolunet.com)
 * @link      http://www.absolunet.com
 */

namespace Absolunet\Exercice9Be\Model\Product\Attribute\Frontend;

use Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend;
use Magento\Framework\DataObject;

/**
 * Warranty attribute frontend
 */
class Warranty extends AbstractFrontend
{
    /**
     * @param \Magento\Framework\DataObject $object
     * @return bool|mixed|string
     */
    public function getValue(DataObject $object)
    {
        $return = parent::getValue($object);
        if (is_string($return)) {
            return '<b>' . parent::getValue($object) . '</b>';
        }
        return $return;
    }
}
