<?php
declare(strict_types=1);

namespace Absolunet\Exercice4Be\Controller\Order;

use Absolunet\Exercice3Be\Controller\Order\View as JsonController;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Controller\AbstractController\OrderViewAuthorizationInterface;

/**
 * Class Order View
 * @package Absolunet\Exercice4Be\Controller\Order
 */
class View extends JsonController
{
    /** @var PageFactory */
    public $resultPageFactory;

    public function __construct(
        RequestInterface $request,
        JsonFactory $resultJsonFactory,
        OrderRepositoryInterface $orderLoader,
        OrderViewAuthorizationInterface $orderViewAuthorization,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($request, $resultJsonFactory, $orderLoader, $orderViewAuthorization);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Get order json
     */
    public function execute(): ResultInterface
    {
        $json = (int)$this->request->getParam('json', null);

        if ($json === 1) {
            return parent::execute();
        }

        $resultPage = $this->resultPageFactory->create();

        /** @var \Magento\Framework\View\Element\Template $orderBlock */
        $orderBlock = $resultPage->getLayout()->getBlock('abso_jsorder_exercice4be');
        $error = [];
        if ($orderBlock) {
            try {
                $order = $this->getOrder();

                $allItems = $order->getAllItems();
                $items = [];
                foreach ($allItems as $item) {
                    $items[] = [
                        'sku' => $item->getSku(),
                        'item_id' => $item->getItemId(),
                        'price' => $item->getPrice()
                    ];
                }

                $orderBlock->setData('orderId', $order->getEntityId());
                $orderBlock->setData('status', $order->getStatus());
                $orderBlock->setData('total', $order->getGrandTotal());
                $orderBlock->setData('products', $items);
                $orderBlock->setData('invoicedTotal', $order->getTotalInvoiced());
            } catch (InputException $e) {
                $error[] = __('No order selected');
            } catch (NoSuchEntityException $e) {
                $error[] = __("Order doesn't exist.");
            }
        }
        $orderBlock->setData('errors', $error);

        return $resultPage;
    }
}
