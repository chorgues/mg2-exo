<?php

namespace Absolunet\Exercice5Be\ViewModel;

use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Serialize\Serializer\Json;

class ExchangeRate implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    const URI = 'https://api.exchangeratesapi.io/latest?base=USD';

    public $curlFactory;
    public $jsonSerializer;
    public $priceCurrency;

    private static $exchangeRate = null;

    public function __construct(
        CurlFactory $curlFactory,
        Json $jsonSerializer,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->curlFactory = $curlFactory;
        $this->jsonSerializer = $jsonSerializer;
        $this->priceCurrency = $priceCurrency;
    }

    public function getExchangeRate()
    {
        if (self::$exchangeRate === null) {
            $curl = $this->curlFactory->create();

            try {
                $curl->get(self::URI);
                $jsonText = $curl->getBody();
                self::$exchangeRate = $this->jsonSerializer->unserialize($jsonText);
            } catch (\Exception $e) {
                return false;
            }
        }

        if (isset(self::$exchangeRate)) {
            return self::$exchangeRate;
        }

        return false;
    }

    public function getPriceCurrency()
    {
        return $this->priceCurrency;
    }
}
